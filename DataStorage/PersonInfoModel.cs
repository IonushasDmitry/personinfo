﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataStorage
{
    public class PersonInfoModel
    {
        [Key]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string ContactPhone { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string Citizenship { get; set; }

        public override string ToString()
        {
            return FirstName + " " + SecondName + " " + LastName + " " 
                + ContactPhone + " " + BirthDate.ToString() + " " + Address + " " + Citizenship;
        }
    }
}
