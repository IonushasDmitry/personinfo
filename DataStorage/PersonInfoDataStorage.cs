﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStorage
{
    public class PersonInfoDataStorage: IDataStorage<PersonInfoModel>
    {
        private readonly PersonInfoContext PersonInfoContext;
        //static PersonInfoDataStorage()
        //{
        //    using (PersonInfoContext db = new PersonInfoContext())
        //    {
        //        List<PersonInfoModel> list = new List<PersonInfoModel>();
        //        list.Add(new PersonInfoModel
        //        {
        //            Id = Guid.NewGuid(),
        //            FirstName = "Dmitry",
        //            SecondName = "Aleksandrovich",
        //            LastName = "Ionushas",
        //            BirthDate = new System.DateTime(1997, 9, 14),
        //            ContactPhone = "29-733-61-50",
        //            Address = "Vitebsk"
        //        });
        //        list.Add(new PersonInfoModel
        //        {
        //            Id = Guid.NewGuid(),
        //            FirstName = "Valery",
        //            SecondName = "Valentinovich",
        //            LastName = "Kardel",
        //            BirthDate = new System.DateTime(1997, 9, 14),
        //            ContactPhone = "29-761-61-50",
        //            Address = "Vitebsk",
        //            Citizenship = "Belarus"
        //        });
        //        list.Add(new PersonInfoModel
        //        {
        //            Id = Guid.NewGuid(),
        //            FirstName = "Ivan",
        //            SecondName = "Sergeevich",
        //            LastName = "Shulgin",
        //            BirthDate = new System.DateTime(1998, 7, 10),
        //            ContactPhone = "29-555-39-43",
        //            Address = "Vitebsk",
        //            Citizenship = "Belarus"
        //        });
        //        list.Add(new PersonInfoModel
        //        {
        //            Id = Guid.NewGuid(),
        //            FirstName = "Natalia",
        //            SecondName = "Aleksandrovna",
        //            LastName = "Galkevich",
        //            BirthDate = new System.DateTime(1998, 2, 27),
        //            ContactPhone = "33-653-15-25",
        //            Address = "Vitebsk",
        //            Citizenship = "Belarus"
        //        });
        //        list.Add(new PersonInfoModel
        //        {
        //            Id = Guid.NewGuid(),
        //            FirstName = "Kristina",
        //            SecondName = "Konstantinovna",
        //            LastName = "Geruckaya",
        //            BirthDate = new System.DateTime(1998, 6, 17),
        //            ContactPhone = "25-444-78-56",
        //            Address = "Vitebsk",
        //            Citizenship = "Belarus"
        //        });


        //        foreach (var record in list)
        //        {
        //            db.PersonInfoRecords.Add(record);
        //            db.SaveChanges();
        //        }
        //    }
        //}

        public PersonInfoDataStorage(PersonInfoContext context)
        {
            this.PersonInfoContext = context;
        }

        public async Task<int> Capacity()
        {
            return await PersonInfoContext.PersonInfoRecords.CountAsync();
        }

        public async Task<List<PersonInfoModel>> GetAll()
        {
            return await PersonInfoContext.PersonInfoRecords.ToListAsync();
        }

        public async Task Add(PersonInfoModel info)
        {
            info.Id = Guid.NewGuid();
            await PersonInfoContext.PersonInfoRecords.AddAsync(info);
            await PersonInfoContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            //Console.WriteLine("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
            //Console.WriteLine("delete: " + id);
            //Console.WriteLine("//////////////////////////////////////");
            //var record = await PersonInfoContext.PersonInfoRecords.AsNoTracking().SingleOrDefaultAsync(m => m.Id == id);
            var record = await PersonInfoContext.PersonInfoRecords.SingleOrDefaultAsync(m => m.Id == id);
            if (record == null)
            {
                return;
            }

            //PersonInfoContext.PersonInfoRecords.Remove(record);
            PersonInfoContext.Entry(record).State = EntityState.Detached;
            PersonInfoContext.Entry(record).State = EntityState.Deleted;
            await PersonInfoContext.SaveChangesAsync();
        }

        public List<PersonInfoModel> Where(Func<PersonInfoModel, bool> predicate)
        {
            return PersonInfoContext.PersonInfoRecords.Where(predicate).ToList();
        }
    }
}
