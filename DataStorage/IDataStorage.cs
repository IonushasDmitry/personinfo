﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataStorage
{
    public interface IDataStorage<T>
    {
        Task<int> Capacity();
        Task<List<T>> GetAll();
        Task Add(T model);
        Task Delete(Guid id);
        List<T> Where(Func<T, bool> predicate);
    }
}
