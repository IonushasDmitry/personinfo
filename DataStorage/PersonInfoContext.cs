﻿using Microsoft.EntityFrameworkCore;

namespace DataStorage
{
    public class PersonInfoContext : DbContext
    {
        public DbSet<PersonInfoModel> PersonInfoRecords { get; set; }

        public PersonInfoContext(DbContextOptions<PersonInfoContext> options)
            : base(options)
        { }
    }
}
