using PersonInfo.Controllers;
using System;
using Xunit;
using DataStorageServices;
using PersonInfo.Paging;
using PersonInfo.Models;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DataStorage;
using Moq;
using System.Collections.Generic;

namespace PersonInfo.Tests
{
    public class HomeControllerTests
    {
        private int RecordsOnPage = 3;
        static HomeControllerTests()
        {
            Mapper.Initialize(
                cfg => {
                    cfg.CreateMap<PersonInfoViewModel, PersonInfoModel>();
                    cfg.CreateMap<PersonInfoModel, PersonInfoViewModel>();
                });
        }

        private IList<PersonInfoModel> GetTestPersonInfoRecords()
        {
            List<PersonInfoModel> list = new List<PersonInfoModel>();
            list.Add(new PersonInfoModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Dmitry",
                SecondName = "Aleksandrovich",
                LastName = "Ionushas",
                BirthDate = new System.DateTime(1997, 9, 14),
                ContactPhone = "29-733-61-50",
                Address = "Vitebsk"
            });
            list.Add(new PersonInfoModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Valery",
                SecondName = "Valentinovich",
                LastName = "Kardel",
                BirthDate = new System.DateTime(1997, 9, 14),
                ContactPhone = "29-761-61-50",
                Address = "Vitebsk",
                Citizenship = "Belarus"
            });
            list.Add(new PersonInfoModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Ivan",
                SecondName = "Sergeevich",
                LastName = "Shulgin",
                BirthDate = new System.DateTime(1998, 7, 10),
                ContactPhone = "29-555-39-43",
                Address = "Vitebsk",
                Citizenship = "Belarus"
            });
            list.Add(new PersonInfoModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Natalia",
                SecondName = "Aleksandrovna",
                LastName = "Galkevich",
                BirthDate = new System.DateTime(1998, 2, 27),
                ContactPhone = "33-653-15-25",
                Address = "Vitebsk",
                Citizenship = "Belarus"
            });
            list.Add(new PersonInfoModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Kristina",
                SecondName = "Konstantinovna",
                LastName = "Geruckaya",
                BirthDate = new System.DateTime(1998, 6, 17),
                ContactPhone = "25-444-78-56",
                Address = "Vitebsk",
                Citizenship = "Belarus"
            });
            return list;
        }

        private PersonInfoViewModel GetValidTestPersonInfoViewModel()
        {
            PersonInfoViewModel model = new PersonInfoViewModel
            {
                Id = Guid.NewGuid(),
                FirstName = "Ivan",
                SecondName = "Ivanovich",
                LastName = "Ivanov",
                ContactPhone = "29-999-11-55",
                BirthDate = new DateTime(1997, 10, 02),
                Address = "Belarus, Vitebsk",
                Citizenship = "Belarus"
            };
            return model;
        }

        [Fact]
        public void IndexReturnsAViewResultWithAListOfPersonInfo()
        {
            var mockStorageServices = new Mock<IDataStorageServices<PersonInfoModel>>();
            mockStorageServices.Setup(services => services.GetAll()).Returns(GetTestPersonInfoRecords());
            HomeController controller = new HomeController(mockStorageServices.Object, new Pager<PersonInfoViewModel>());

            IActionResult result = controller.Index(null, null);

            ViewResult viewResult = Assert.IsType<ViewResult>(result);
            IList<PersonInfoViewModel> model = Assert.IsAssignableFrom<IList<PersonInfoViewModel>>(viewResult.Model);
            Assert.Equal(model.Count, RecordsOnPage);
            //Assert.InRange(model.Count, 0, RecordsOnPage);
        }

        [Fact]
        public void AddInfoWithValidModelReturnRedirect()
        {
            var mockStorage = new Mock<IDataStorageServices<PersonInfoModel>>();
            HomeController controller = new HomeController(mockStorage.Object, new Pager<PersonInfoViewModel>());

            PersonInfoViewModel model = GetValidTestPersonInfoViewModel();

            IActionResult result = controller.AddInfo(model);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public void AddInfoWithInvalidModelReturnViewResult()
        {
            var mockStorage = new Mock<IDataStorageServices<PersonInfoModel>>();
            HomeController controller = new HomeController(mockStorage.Object, new Pager<PersonInfoViewModel>());
            controller.ModelState.AddModelError("LastName", "Required");

            PersonInfoViewModel model = GetValidTestPersonInfoViewModel();
            model.LastName = "";

            IActionResult result = controller.AddInfo(model);

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void AddInfoReturnsViewResult()
        {
            var mockStorageServices = new Mock<IDataStorageServices<PersonInfoModel>>();
            HomeController controller = new HomeController(mockStorageServices.Object, new Pager<PersonInfoViewModel>());

            IActionResult result = controller.AddInfo();

            Assert.IsType<ViewResult>(result);
        }
    }
}
