﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataStorage;
using System;

namespace DataStorageServices
{
    public class PersonInfoDataStorageServices: IDataStorageServices<PersonInfoModel>
    {
        private IDataStorage<PersonInfoModel> Storage;

        public PersonInfoDataStorageServices(IDataStorage<PersonInfoModel> Storage)
        {
            this.Storage = Storage;
        }

        public async Task<IList<PersonInfoModel>> GetAll()
        {
            return await Storage.GetAll();
        }
        
        public async Task<int> GetCapacity()
        {
            return await Storage.Capacity();
        }

        public async Task Add(PersonInfoModel record)
        {
            await Storage.Add(record);
        }

        public async Task Delete(Guid id)
        {
            await Storage.Delete(id);
        }

        public List<PersonInfoModel> Where(Func<PersonInfoModel, bool> predicate)
        {
            return Storage.Where(predicate);
        }
    }
}
