﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataStorageServices
{
    public interface IDataStorageServices<T>
    {
        Task<int> GetCapacity();
        Task<IList<T>> GetAll();
        Task Add(T record);
        Task Delete(Guid id);
        List<T> Where(Func<T, bool> predicate);
    }
}
