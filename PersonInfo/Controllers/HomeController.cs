﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PersonInfo.Models;
using AutoMapper;
using DataStorage;
using PersonInfo.Paging;
using PersonInfo.Sorting;
using DataStorageServices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PersonInfo.Controllers
{
    public class HomeController : Controller
    {
        private int RecordsOnPage = 3;
        private IPager<PersonInfoViewModel> Pager;
        private IDataStorageServices<PersonInfoModel> StorageServices;

        public HomeController(IDataStorageServices<PersonInfoModel> _services, IPager<PersonInfoViewModel> _pager)
        {
            StorageServices = _services;
            Pager = _pager;
            Pager.SetRecordsOnPage(RecordsOnPage);
        }

        private async Task<IList<PersonInfoViewModel>> GetRecordsFromDB()
        {
            IList<PersonInfoModel> modelList = await StorageServices.GetAll();
            IList<PersonInfoViewModel> viewModelList = new List<PersonInfoViewModel>();
            foreach (PersonInfoModel info in modelList)
            {
                viewModelList.Add(Mapper.Map<PersonInfoViewModel>(info));
            }
            return viewModelList;
        }

        private IList<PersonInfoViewModel> GetSortedList(IList<PersonInfoViewModel> records, PersonInfoSortOrder sortOrder)
        {
            PersonInfoSorter sorter = new PersonInfoSorter();
            sorter.SetSortOrder(sortOrder);
            sorter.SetRecords(records);
            return sorter.GetSortedList();
        }

        public async Task<IActionResult> Index(int? page, PersonInfoSortOrder? sortOrder)
        {
            int pageNumber = page ?? 1;
            PersonInfoSortOrder order = sortOrder ?? PersonInfoSortOrder.LastNameAsc;

            ViewData["LastNameOrder"]  = order == PersonInfoSortOrder.LastNameAsc ? PersonInfoSortOrder.LastNameDesc : PersonInfoSortOrder.LastNameAsc;
            ViewData["BirthDateOrder"] = order == PersonInfoSortOrder.BirthDateAsc ? PersonInfoSortOrder.BirthDateDesc : PersonInfoSortOrder.BirthDateAsc;

            IList<PersonInfoViewModel> viewModelList = await GetRecordsFromDB();

            Pager.SetRecordsOnPage(RecordsOnPage);
            Pager.SetRecords(GetSortedList(viewModelList, order));

            ViewBag.SortOrder = order;
            ViewBag.CountOfPages = Pager.GetCountOfPages();
            ViewBag.CurrentPage = pageNumber;

            return View(Pager.GetPage(pageNumber));
        }

        [HttpGet]
        public IActionResult AddInfo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddInfo(PersonInfoViewModel personInfo)
        {
            if (ModelState.IsValid)
            {
                PersonInfoModel info = Mapper.Map<PersonInfoModel>(personInfo);
                await StorageServices.Add(info);
                return RedirectToAction("Index");
            }
            ViewBag.InvalidData = new bool?(true);
            return View();
        }
        
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
