﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PersonInfo.Models
{
    public class PersonInfoViewModel
    {
        public Guid Id { get; set; }

        [Required (ErrorMessage = "First name is required")]
        [Display(Name = "First name")]
        [RegularExpression(@"[a-zA-Z]+", ErrorMessage = "The name must consist only of letters")]
        [StringLength(30, ErrorMessage = "The name can not be longer than 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Second name is required")]
        [Display(Name = "Second name")]
        [RegularExpression(@"[a-zA-Z]+", ErrorMessage = "The name must consist only of letters")]
        [StringLength(30, ErrorMessage = "The name can not be longer than 30 characters")]
        public string SecondName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last name")]
        [RegularExpression(@"[a-zA-Z]+", ErrorMessage = "The name must consist only of letters")]
        [StringLength(30, ErrorMessage = "The name can not be longer than 30 characters")]
        public string LastName { get; set; }

        [Display(Name = "Contact phone (+375-##-###-##-##)")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{2}", ErrorMessage = "Invalid phone number")]
        public string ContactPhone { get; set; }

        [Required (ErrorMessage = "Birth date is required")]
        [Display(Name = "Birth date")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Required (ErrorMessage = "Address is required")]
        [Display(Name = "Address")]
        [StringLength(50, ErrorMessage = "The address can not be longer than 50 characters")]
        public string Address { get; set; }

        [Required (ErrorMessage = "Citizenship is required")]
        [StringLength(30, ErrorMessage = "The citizenship can not be longer than 30 characters")]
        public string Citizenship { get; set; }
    }
}
