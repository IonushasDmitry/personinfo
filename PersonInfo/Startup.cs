﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using DataStorageServices;
using DataStorage;
using PersonInfo.Paging;
using PersonInfo.Models;

namespace PersonInfo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(
                cfg => {
                    cfg.CreateMap<PersonInfoViewModel, PersonInfoModel>();
                    cfg.CreateMap<PersonInfoModel, PersonInfoViewModel>();
            });


            services.AddDbContext<PersonInfoContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IDataStorage<PersonInfoModel>, PersonInfoDataStorage>();
            services.AddTransient<IDataStorageServices<PersonInfoModel>, PersonInfoDataStorageServices>();
            services.AddTransient<IPager<PersonInfoViewModel>, Pager<PersonInfoViewModel>>();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
