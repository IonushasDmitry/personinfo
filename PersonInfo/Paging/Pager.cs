﻿using System;
using System.Collections.Generic;

namespace PersonInfo.Paging
{
    public class Pager<T>: IPager<T>
    {
        private int RecordsOnPage = 5;
        private IList<T> Records = new List<T>();

        public void SetRecordsOnPage(int recordsOnPage)
        {
            if (recordsOnPage <= 0)
            {
                throw new ArgumentException();
            }
            RecordsOnPage = recordsOnPage;
        }

        public void SetRecords(IList<T> Records)
        {
            this.Records = Records;
        }

        public int GetRecordsOnPage()
        {
            return RecordsOnPage;
        }

        public int GetCountOfPages()
        {
            int countOfRecords = Records.Count;
            int pages = countOfRecords / RecordsOnPage;
            if (countOfRecords % RecordsOnPage > 0)
            {
                pages++;
            }
            return pages;
        }

        public IList<T> GetPage(int pageNumber)
        {
            if (pageNumber > GetCountOfPages() || pageNumber < 1)
            {
                throw new ArgumentException();
            }

            int firstIndex = (pageNumber - 1) * RecordsOnPage;
            int lastIndex = pageNumber * RecordsOnPage;
            if (lastIndex > Records.Count)
            {
                lastIndex = Records.Count;
            }

            IList<T> page = new List<T>();
            for (int i = firstIndex; i < lastIndex; i++)
            {
                page.Add(Records[i]);
            }

            return page;
        }

        public bool HasPage(int pageNumber)
        {
            if (pageNumber > 0 && pageNumber <= GetCountOfPages())
            {
                return true;
            }
            return false;
        }
    }
}
