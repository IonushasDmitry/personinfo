﻿using System.Collections.Generic;
namespace PersonInfo.Paging
{
    public interface IPager<T>
    {
        void SetRecordsOnPage(int recordsOnPage);
        void SetRecords(IList<T> Records);
        int GetCountOfPages();
        IList<T> GetPage(int pageNumber);
        bool HasPage(int pageNumber);
    }
}
