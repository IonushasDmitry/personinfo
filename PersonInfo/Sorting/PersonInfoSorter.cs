﻿using PersonInfo.Models;
using System.Collections.Generic;
using System.Linq;

namespace PersonInfo.Sorting
{
    public class PersonInfoSorter
    {
        private IList<PersonInfoViewModel> Records = new List<PersonInfoViewModel>();
        private PersonInfoSortOrder SortOrder = PersonInfoSortOrder.LastNameAsc;

        public void SetSortOrder(PersonInfoSortOrder SortOrder)
        {
            this.SortOrder = SortOrder;
        }

        public PersonInfoSortOrder GetSortOrder()
        {
            return SortOrder;
        }

        public void SetRecords(IList<PersonInfoViewModel> Records)
        {
            this.Records = Records;
        }

        public IList<PersonInfoViewModel> GetSortedList()
        {
            if (SortOrder == PersonInfoSortOrder.BirthDateAsc)
            {
                Records = Records.OrderBy(x => x.BirthDate).ToList();
            }
            if (SortOrder == PersonInfoSortOrder.BirthDateDesc)
            {
                Records = Records.OrderBy(x => x.BirthDate).ToList();
                Records = Records.Reverse().ToList();
            }
            if (SortOrder == PersonInfoSortOrder.LastNameAsc)
            {
                Records = Records.OrderBy(x => x.LastName).ToList();
            }
            if (SortOrder == PersonInfoSortOrder.LastNameDesc)
            {
                Records = Records.OrderBy(x => x.LastName).ToList();
                Records = Records.Reverse().ToList();
            }

            return Records;
        }
    }
}
