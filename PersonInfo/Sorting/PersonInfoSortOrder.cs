﻿namespace PersonInfo.Sorting
{
    public enum PersonInfoSortOrder
    {
        LastNameAsc,
        LastNameDesc,
        BirthDateAsc,
        BirthDateDesc
    }
}
