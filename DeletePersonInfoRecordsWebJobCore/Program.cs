﻿using DataStorage;
using DataStorageServices;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace DeletePersonInfoRecordsWebJobCore
{
    class Program
    {
        private static IConfigurationRoot Configuration;

        static void Main()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var configuration = new JobHostConfiguration();
            configuration.UseDevelopmentSettings();
            configuration.JobActivator = new CustomJobActivator(serviceCollection.BuildServiceProvider());
            configuration.UseTimers();

            var host = new JobHost(configuration);
            host.RunAndBlock();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PersonInfoContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IDataStorage<PersonInfoModel>, PersonInfoDataStorage>();
            services.AddTransient<IDataStorageServices<PersonInfoModel>, PersonInfoDataStorageServices>();
            services.AddTransient<WebJobsMethods, WebJobsMethods>();

            Environment.SetEnvironmentVariable("AzureWebJobsDashboard", Configuration["AzureWebJobsDashboard"]);
            Environment.SetEnvironmentVariable("AzureWebJobsStorage", Configuration["AzureWebJobsStorage"]);
        }
    }
}
