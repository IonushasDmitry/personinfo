﻿using DataStorage;
using DataStorageServices;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Linq;

namespace DeletePersonInfoRecordsWebJobCore
{
    public class WebJobsMethods
    {
        public string EmailName { get; set; }
        public string EmailPassword { get; set; }
        public string EmailSenderName { get; set; }
        public string EmailReceiverName { get; set; }
        private readonly IDataStorageServices<PersonInfoModel> Services;

        public WebJobsMethods(IDataStorageServices<PersonInfoModel> Services)
        {
            this.Services = Services;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var Configuration = builder.Build();
            EmailName = Configuration["EmailName"];
            EmailPassword = Configuration["EmailPassword"];
            EmailSenderName = Configuration["EmailSenderName"];
            EmailReceiverName = Configuration["EmailReceiverName"];
        }
        
        // The timer is triggered once a minute
        public async Task DeleteRecordsWithouthPhone([TimerTrigger("0 */1 * * * *")]TimerInfo timer, TextWriter log)
        {
            Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            Console.WriteLine("It's time to delete some records :)");
            Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

            IList<PersonInfoModel> recordsToDelete = Services.Where(m => string.IsNullOrEmpty(m.ContactPhone));

            foreach(var record in recordsToDelete)
            {
                SendEmail(record.Id.ToString());
                await Services.Delete(record.Id);
            }
        }

        private void SendEmail(string message)
        {
            Console.WriteLine("================================================================");
            Console.WriteLine("Send message");
            Console.WriteLine("================================================================");
            MailMessage emailMessage = new MailMessage();
            emailMessage.To.Add(new MailAddress(EmailName, "Dmitry"));
            emailMessage.From = new MailAddress(EmailName, "Dmitry");
            emailMessage.Subject = "Azure Web App Email";
            emailMessage.Body = "Message from PersonInfo WebJob \nDelete record from database: " + message + "\n";
            emailMessage.IsBodyHtml = true;
            SmtpClient client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(EmailName, EmailPassword),
                Port = 587,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };
            try
            {
                client.Send(emailMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
